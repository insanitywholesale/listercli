module gitlab.com/insanitywholesale/listercli

go 1.16

require (
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
	gitlab.com/insanitywholesale/lister v0.0.0-20210704114516-f244109573fd
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	google.golang.org/genproto v0.0.0-20210701191553-46259e63a0a9 // indirect
	google.golang.org/grpc v1.39.0 // indirect
)
