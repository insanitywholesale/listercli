package cmd

import (
	"fmt"
	"io"
	"encoding/json"
	"net/http"
	pb "gitlab.com/insanitywholesale/lister/proto/v1"

	"github.com/spf13/cobra"
)

// getlistsCmd represents the getlists command
var getlistsCmd = &cobra.Command{
	Use:   "getlists",
	Short: "Retrieve all lists",
/*
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
*/
	Run: func(cmd *cobra.Command, args []string) {
		getLists(args)
	},
}

func init() {
	rootCmd.AddCommand(getlistsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getlistsCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getlistsCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func getLists(args []string) {
	responsejson, err := http.Get(url+"/lists")
	if err != nil {
		fmt.Errorf("Error while retrieving the lists %v", err)
	}

	lists := &pb.Lists{}
	response, err := io.ReadAll(responsejson.Body)
	responsejson.Body.Close()
	if err != nil {
		fmt.Errorf("Error while reading the response %v", err)
	}
	_ = json.Unmarshal(response, &lists)

	fmt.Println(lists)
	for _, l := range lists.Lists {
		fmt.Printf("%v: %s\n", l.Id, l.Title)
		for _, i := range l.Items {
			fmt.Printf("\t%v\n", i)
		}
	}
}
